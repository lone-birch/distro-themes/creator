// noinspection UnnecessaryLocalVariableJS

import type { FetchError } from 'ofetch';
import type { DistroProduct, ProductVariant } from './types';

export function capitalize(word: any) {
  let value = word.toString().toLowerCase();
  return value.toLowerCase().charAt(0).toUpperCase() + value.slice(1);
}

export function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export function sort(object: any) {
  const values = Object.values(object);
  return values.map((v: any) => v.toString()).sort();
}

export function getFilteredProductVariants(product: DistroProduct): ProductVariant[] {
  // Check if product is not null and has a variants property that is an array
  if (!product || !Array.isArray(product.variants)) {
    return []; // Return an empty array if the product is null or doesn't have a valid variants array
  }

  const variants = product.variants.filter((variant: any) => {
    return (
      (variant.inventory?.madeToOrder === true ||
        (variant.inventory?.madeToOrder === false && variant.inventory.inStock > 0)) &&
      variant.price !== null
    );
  });

  return variants;
}

export function getFetchErrorMessage(error: FetchError<any>) {
  const { data, statusCode, statusMessage } = error;
  if (typeof data === 'string') return data;
  if (data.message) return data.message;
  if (statusCode && statusCode >= 400 && statusCode < 500) return 'Invalid request.';
  if (statusCode && statusCode >= 500) return 'Something went wrong on our end.';
  return statusMessage ?? 'Something went wrong on our end.';
}
