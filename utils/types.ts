export interface CartItem {
  quantity: number;
  productId?: string;
  isVariant?: boolean;
  productName?: string;
  id: string;
  name?: string;
  price: number;
  thumbnail?: string;
  variantCategories?: Record<string, string>;
}

export interface Organization {
  name: string;
  brand?: {
    logo: string;
    facebook: string;
    instagram: string;
    twitter: string;
    pinterest: string;
  };
  website: {
    active: boolean;
  };
}

export interface DistroProduct {
  id: string;
  payout: Payout;
  organizationId: string;
  shipping: Shipping;
  inventory: Inventory;
  price: string;
  sku: string;
  docsImages: DocsImage[];
  dimensions: Dimensions;
  weight: Weight;
  variants: ProductVariant[];
  variantCategories: VariantCategory[];
  documentation: string;
  active: boolean;
  name: string;
  description: string;
  thumbnail: string;
  images: DocsImage[];
  displayVariantsAsProducts: boolean;
  reviews: Review[];
}

export interface ProductVariant {
  id: string;
  parentProductId: string | null;
  name: string;
  sku: string;
  productId: string;
  thumbnail: string;
  price: number;
  active: boolean;
  boardFeet: number | null;
  inventory: Inventory;
  shipping: Shipping;
  weight: Weight;
  dimensions: Dimensions;
  payout: Payout;
  variantCategories: Record<string, string>;
}

export type Inventory = {
  madeToOrder: boolean;
  inStock: number;
};

export type Payout = {
  fee: number | null;
  bonus: number | null;
};

export type Shipping = {
  service: string;
  free: boolean;
};

export type DocsImage = {
  name: string;
  url: string;
};

export type Message = {
  name: string;
  email: string;
  message: string;
};

export type Review = {
  created: string;
  productId: string;
  orderId: string;
  review: string;
  stars: number;
};

export type Dimensions = {
  width: number;
  unit: string;
  height: number;
  length: number;
};

export type Weight = {
  unit: string;
  value: number;
};

export type SubCategory = {
  name: string;
  selected?: boolean;
};

export type VariantCategory = {
  name: string;
  subCategories: SubCategory[];
};
