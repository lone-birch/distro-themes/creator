import { useRuntimeConfig } from '#imports';
import type { Review } from '~/utils/types';

export async function sendProductReview(body: Review) {
  const { apiRoot } = useRuntimeConfig().public;
  const url = `${apiRoot}/orders/${body.orderId}/review`;

  try {
    const response = await $fetch.raw(url, {
      method: 'POST',
      body,
    });

    const data = (await response.json()) as { message: string };
    return { success: true, data };
  } catch (error) {
    // Type-check the error
    if (error instanceof Error) {
      console.error('Error in postReview:', error.message);
      return { success: false, error: error.message };
    } else {
      // Handle cases where the error is not an instance of Error
      console.error('An unknown error occurred in postReview');
      return { success: false, error: 'An unknown error occurred' };
    }
  }
}
