import type { Message } from '~/utils/types';
import { useFetch, useRuntimeConfig } from '#imports';

export async function sendContact(message: Message) {
  const { apiRoot, orgId } = useRuntimeConfig().public;
  const url = `${apiRoot}/sites/${orgId}/contact`;

  try {
    const fetchResult = await useFetch(url, {
      method: 'post',
      body: message,
    });

    if (fetchResult.error.value) {
      // Assume fetchResult.error.value is an object with a message property
      // You might need to adjust this depending on the actual shape of your error object
      throw new Error(fetchResult.error.value.message || 'Unknown error occurred');
    }

    return { success: true, data: fetchResult.data.value };
  } catch (error) {
    // Type-check the error
    if (error instanceof Error) {
      console.error('Error in contactUs:', error.message);
      return { success: false, error: error.message };
    } else {
      // Handle cases where the error is not an instance of Error
      console.error('An unknown error occurred in postReview');
      return { success: false, error: 'An unknown error occurred' };
    }
  }
}
