import { useFetch, useRuntimeConfig } from '#imports';
import type { Ref } from '#imports';
import type { Organization } from '~/utils/types';

export async function getOrg() {
  const { apiRoot, orgId } = useRuntimeConfig().public;
  const url = `${apiRoot}/sites/${orgId}/organization`;

  try {
    const { data, error } = await useFetch(url, {
      method: 'get',
    });

    if (error.value) {
      // Assume fetchResult.error.value is an object with a message property
      // You might need to adjust this depending on the actual shape of your error object
      throw new Error(error.value.message || 'Unknown error occurred');
    }

    return { success: true, error: error, data: data as Ref<Organization> };
  } catch (error) {
    // Type-check the error
    if (error instanceof Error) {
      console.error('Error in contactUs:', error.message);
      return { success: false, error: error.message };
    } else {
      // Handle cases where the error is not an instance of Error
      console.error('An unknown error occurred in postReview');
      return { success: false, error: 'An unknown error occurred' };
    }
  }
}
