const { API_URL, SITE_ORG_ID, SITE_DOMAIN, SITE_ORG_STRIPE_CONNECT_ACCOUNT } =
  process.env as Record<string, string>;
if (!API_URL || !SITE_ORG_ID || !SITE_DOMAIN || !SITE_ORG_STRIPE_CONNECT_ACCOUNT) {
  throw new Error('Missing environment variables.');
}

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  imports: {
    autoImport: false,
  },
  modules: ['@nuxt/content', '@pinia/nuxt', '@pinia-plugin-persistedstate/nuxt'],
  css: ['~/assets/css/main.scss'],
  postcss: {
    plugins: {
      'postcss-import': {},
      'tailwindcss/nesting': {},
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  runtimeConfig: {
    public: {
      apiRoot: API_URL,
      orgId: SITE_ORG_ID,
      domain: SITE_DOMAIN,
      connectAccountId: SITE_ORG_STRIPE_CONNECT_ACCOUNT,
    },
  },
  piniaPersistedstate: {
    cookieOptions: {
      sameSite: 'strict',
    },
    storage: 'localStorage',
  },
});
