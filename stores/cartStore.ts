import { defineStore } from 'pinia';
import type { CartItem } from '~/utils/types';

export const useCartStore = defineStore('cart', {
  state: () => {
    return {
      cart: [] as CartItem[],
    };
  },
  actions: {
    async addToCart(item: CartItem) {
      this.cart.push(item);
    },
    emptyCart() {
      this.cart = [];
    },
    removeFromCart(item: CartItem) {
      this.cart.splice(this.cart.indexOf(item), 1);
    },
  },
  persist: true,
});
