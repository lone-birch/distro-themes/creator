import { defineStore } from 'pinia';
import type { PageContentType } from '@distromade/distro-cms/dist/types';

export interface ContentState {
  content: PageContentType | null;
}

export const useContentStore = defineStore('page-content', {
  state(): ContentState {
    return {
      content: null as PageContentType | null,
    };
  },
  actions: {
    setContent(content: PageContentType | null) {
      this.content = content;
    },
  },
});
