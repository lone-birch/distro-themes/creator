import { defineStore } from 'pinia';
import * as API from '~/utils/api';
import type { Organization } from '~/utils/types';

export const useOrgStore = defineStore('organization', {
  state: () => ({
    org: null as Organization | null,
  }),
  actions: {
    setOrg(org: Organization | null) {
      this.org = org;
    },
    async fetchOrg() {
      try {
        const { data } = await API.getOrg();
        this.setOrg(data?.value ?? null);
        return data?.value;
      } catch (error) {
        console.error('Error fetching organization:', error);
        throw error;
      }
    },
  },
  persist: true,
});
