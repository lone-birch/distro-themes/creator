// noinspection BadExpressionStatementJS

import { v4 as uuid } from 'uuid';
import { useGlobalCMSState } from '@distromade/distro-cms';
import type {
  MarkdownElement,
  PlainTextElement,
  PageContentType,
} from '@distromade/distro-cms/dist/types';
import { queryContent, computed } from '#imports';
import { useContentStore } from '~/stores';

export async function useThemeContent<
  M = Record<string, MarkdownElement>,
  P = Record<string, PlainTextElement>,
>(contentPath: string) {
  const { pageContent } = useGlobalCMSState();
  const contentStore = useContentStore();

  const content = (await queryContent(contentPath).findOne()) as unknown as PageContentType;
  contentStore.setContent(content);

  const mdElements = computed(() => {
    const { markdownElements } = content.cms;
    const { markdownElements: cmsMdElements } = JSON.parse(
      JSON.stringify(pageContent.value?.cms ?? {}),
    );
    if (cmsMdElements) return cmsMdElements as M;
    return markdownElements as M;
  });

  const textElements = computed(() => {
    const { plainTextElements } = content.cms;
    const { plainTextElements: cmsTextElements } = JSON.parse(
      JSON.stringify(pageContent.value?.cms ?? {}),
    );
    if (cmsTextElements) return cmsTextElements as P;
    return plainTextElements as P;
  });

  const pageTitle = computed<string>(() => {
    if (pageContent.value) return pageContent.value.cms.pageTitle;
    return content.cms.pageTitle;
  });

  /**
   * This is a hack to force elements bound to mdElements via v-html to
   * re-render when the pageContent changes.
   */
  const key = computed(() => {
    // does nothing; just triggers reactivity:
    pageContent.value?.cms;
    return uuid();
  });

  return {
    key,
    mdElements,
    textElements,
    pageTitle,
  };
}
