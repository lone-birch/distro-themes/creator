import { useAsyncData, useRuntimeConfig } from '#imports';
import { getFilteredProductVariants } from '~/utils';
import type { DistroProduct } from '~/utils/types';

export async function useActiveProducts() {
  const {
    data: products,
    pending,
    error,
  } = await useAsyncData('active-products', async () => {
    const { apiRoot, orgId } = useRuntimeConfig().public;
    const url = `${apiRoot}/organizations/${orgId}/products?active=true`;
    const response = await fetch(url);
    if (!response.ok) {
      console.log('useActiveProducts:', response.status, response.statusText);
      throw createError({ statusCode: response.status, message: response.statusText });
    }

    const __products = JSON.parse(JSON.stringify(await response.json()));
    return __products as DistroProduct[];
  });

  products.value = (products.value ?? []).map((product) => {
    const variants = getFilteredProductVariants(product);
    return { ...product, variants };
  });

  return { pending, error, products };
}
