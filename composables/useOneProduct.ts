import { useAsyncData, useRuntimeConfig, ref } from '#imports';
import { getFilteredProductVariants } from '~/utils';
import type { Ref } from '#imports';
import type { NuxtError } from '#app';
import type { DistroProduct } from '~/utils/types';

export async function useOneProduct(productId: string) {
  const {
    data: __product,
    pending,
    error,
  } = await useAsyncData(`get-product-${productId}`, async () => {
    const { apiRoot, orgId } = useRuntimeConfig().public;
    const url = `${apiRoot}/organizations/${orgId}/products/${productId}?activeVariants=true`;
    const response = await fetch(url);
    if (!response.ok) {
      throw createError({ statusCode: 404, message: `Product does not exist.` });
    }
    return (await response.json()) as DistroProduct;
  });

  __product.value = {
    ...__product.value!,
    variants: getFilteredProductVariants(__product.value!),
  };

  return {
    pending,
    error: error as unknown as Ref<NuxtError | null>,
    product: ref(__product.value!),
  };
}
